import tkinter as tk 
from tkinter import filedialog, Text
import os
from named_entity import train_entities
from named_entity import output
from tkinter import messagebox

apps = []
root = tk.Tk()

def runScript():
    entity = train_entities()
    apps.append(entity)
    
def final_result():
    fr = output()
    apps.append(fr)

    for app in apps:
         my_label = tk.Label(frame, text = app, width = 400, height = 60, bg="white")
         my_label.config(wraplength = 200)
         my_label.pack()

HEIGHT = 500
WIDTH = 500

canvas = tk.Canvas(root, height=700, width= 700, bg="#263D42")
canvas.pack()

frame = tk.Frame(root, bg="blue")
frame.place(relx=0.1, rely=0.1, relwidth=0.5, relheight=0.5)

runApp = tk.Button(root, text="start training",padx=10, pady=5, bg='grey', command=final_result)
runApp.pack()

root.mainloop()